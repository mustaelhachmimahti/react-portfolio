import './style/App.css';
import {
    Button,
    Carousel,
    Container,
    Form,
    FormControl,
    Nav,
    Navbar,
    NavDropdown,
    Offcanvas,
    ProgressBar
} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import React from "react"
import {MDBCol, MDBContainer, MDBFooter, MDBRow} from 'mdb-react-ui-kit'


function Presentation() {
    return (
        <div class="presentation-container">
            <h1>Welcome to my presentation</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et tincidunt sapien. Praesent nec neque
                sem. Maecenas ullamcorper, ipsum eu cursus tempus, nunc nunc fermentum mi, non congue eros augue eget
                metus.</p>
        </div>
    )
}

class Progres extends React.Component {
    render() {
        const now = 60;
        const progressInstance = <ProgressBar variant="success" animated now={now}/>;
        return progressInstance
    }
}

class Carou extends React.Component {
    render() {
        return (
            <Carousel>
                <Carousel.Item interval={1000}>
                    <img
                        className="d-block w-100"
                        src="./../image/project1.jpg"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item interval={500}>
                    <img
                        className="d-block w-100"
                        src="./../image/project2.jpg"
                        alt="Second slide"
                    />
                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="./../image/project3.jpg"
                        alt="Third slide"
                    />
                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        )
    }
}

class Main extends React.Component {
    render() {
        return <div class="inputs-form">

            <Form>
                <Form.Group className="mb-3" controlId="Mustapha El Hachmi Mahti">
                    <Form.Label>Full name</Form.Label>
                    <Form.Control type="text" placeholder="Mustapha El Hachmi Mahti"/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="name@example.com"/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Example textarea</Form.Label>
                    <Form.Control as="textarea" placeholder="Message" rows={3}/>
                </Form.Group>
                <Button type="submit" variant="primary">Send</Button>{' '}
            </Form>
        </div>
    }
}


function Header() {
    return (

        <Navbar expand={false}>
            <Container fluid>
                <Navbar.Brand href="#">Mustapha El Hachmi Mahti</Navbar.Brand>
                <Navbar.Toggle aria-controls="offcanvasNavbar"/>
                <Navbar.Offcanvas
                    id="offcanvasNavbar"
                    aria-labelledby="offcanvasNavbarLabel"
                    placement="end"
                >
                    <Offcanvas.Header closeButton>
                        <Offcanvas.Title id="offcanvasNavbarLabel">Offcanvas</Offcanvas.Title>
                    </Offcanvas.Header>
                    <Offcanvas.Body>
                        <Nav className="justify-content-end flex-grow-1 pe-3">
                            <Nav.Link href="#action1">Home</Nav.Link>
                            <Nav.Link href="#action2">Link</Nav.Link>
                            <NavDropdown title="Dropdown" id="offcanvasNavbarDropdown">
                                <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
                                <NavDropdown.Item href="#action4">Another action</NavDropdown.Item>
                                <NavDropdown.Divider/>
                                <NavDropdown.Item href="#action5">
                                    Something else here
                                </NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                        <Form className="d-flex">
                            <FormControl
                                type="search"
                                placeholder="Search"
                                className="me-2"
                                aria-label="Search"
                            />
                            <Button variant="outline-success">Search</Button>
                        </Form>
                    </Offcanvas.Body>
                </Navbar.Offcanvas>
            </Container>
        </Navbar>

    )
}

function Footer() {
    return (
        <MDBFooter backgroundColor='light' className='text-center text-lg-left'>
            <MDBContainer className='p-4'>
                <MDBRow>
                    <MDBCol lg='3' md='6' className='mb-4 mb-md-0'>
                        <h5 className='text-uppercase'>Links</h5>

                        <ul className='list-unstyled mb-0'>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 1
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 2
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 3
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 4
                                </a>
                            </li>
                        </ul>
                    </MDBCol>

                    <MDBCol lg='3' md='6' className='mb-4 mb-md-0'>
                        <h5 className='text-uppercase mb-0'>Links</h5>

                        <ul className='list-unstyled'>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 1
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 2
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 3
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 4
                                </a>
                            </li>
                        </ul>
                    </MDBCol>

                    <MDBCol lg='3' md='6' className='mb-4 mb-md-0'>
                        <h5 className='text-uppercase'>Links</h5>

                        <ul className='list-unstyled mb-0'>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 1
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 2
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 3
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 4
                                </a>
                            </li>
                        </ul>
                    </MDBCol>

                    <MDBCol lg='3' md='6' className='mb-4 mb-md-0'>
                        <h5 className='text-uppercase mb-0'>Links</h5>

                        <ul className='list-unstyled'>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 1
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 2
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 3
                                </a>
                            </li>
                            <li>
                                <a href='#!' className='text-dark'>
                                    Link 4
                                </a>
                            </li>
                        </ul>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>

            <div className='text-center p-3' style={{backgroundColor: 'orange'}}>
                &copy; {new Date().getFullYear()} Copyright:{' '}
                <a className='text-dark' href='https://mdbootstrap.com/'>
                    MDBootstrap.com
                </a>
            </div>
        </MDBFooter>
    );

}

function App() {
    return (
        <div><Header/>
            <Progres/>
            <Presentation/>
            <Carou/>
            <Main/>
            <Footer/>

        </div>
    )
}

export default App;
